## Overview

Photo Sphere Viewer (https://photo-sphere-viewer.js.org/) is a JavaScript library that allows you to display 360×180 degrees panoramas on any web page.


## Requirements

This module depends on the core Image module a
The Photo Sphere Viewer plugin is used for the Panorama effects.


## Install

Run `npm install` in Photo Sphere Viewer module.


## Configuration

To configure the Image Photo Sphere display, go to Administration > Structure > Content types and select the content type you would want to use. If you do not already have an Image field defined, add one by going to the Manage Fields tab. After you have an Image field, go to the Manage Display tab. Change the Format for your Image field to Photo sphere viewer Image. To change which styles are displayed for the displayed image, select the desired image styles, and click Update.')