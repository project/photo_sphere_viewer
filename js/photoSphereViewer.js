(function ($, Drupal, drupalSettings) {
    'use strict';

    /**
     * Initialize Photo Sphere viewer.
     */
    Drupal.behaviors.photosphere = {
        attach: function (context, drupalSettings) {
            $('#photosphere').once('photosphere').each(
                function () {
                    var imgSrc = drupalSettings.panoramaImageUrl;
                    const Viewer = new PhotoSphereViewer.Viewer(
                        {
                            container: 'photosphere',
                            panorama: imgSrc,
                            defaultLat: 0.3,
                            mousewheel: drupalSettings.enable_mousewheel,
                        }
                    );
                    if (drupalSettings.use_title_as_caption) {
                          Viewer.navbar.setCaption(drupalSettings.image_title);
                    }
                    if (!(drupalSettings.show_navbar)) {
                        Viewer.navbar.container.style.visibility = "hidden";
                    }
                }
            );

        }
    };
})(jQuery, Drupal, drupalSettings);
