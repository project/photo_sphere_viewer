<?php

namespace Drupal\photo_sphere_viewer\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\image\Entity\ImageStyle;

/**
 * Plugin implementation of the 'virtual_tour' formatter.
 *
 * @FieldFormatter(
 *  id = "photo_sphere_viewer",
 *  label = @Translation("Photo sphere viewer"),
 *  field_types = {
 *     "image"
 *  },
 *  quickedit = {
 *    "editor" = "image"
 *  }
 * )
 */
class PhotoSphereFormatter extends ImageFormatterBase
{

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings()
    {
        return [
        'photo_sphere_viewer_image_style' => '',
        'show_navbar' => '1',
        'enable_mousewheel' => '1',
        'use_title_as_caption' => '1',
        ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state)
    {
        $image_styles = image_style_options(false);
        $description_link = Link::fromTextAndUrl(
            $this->t('Configure Image Styles'),
            Url::fromRoute('entity.image_style.collection')
        );

        $element['photo_sphere_viewer_image_style'] = [
        '#type' => 'select',
        '#title' => $this->t('Image style'),
        '#options' => $image_styles,
        '#default_value' => $this->getSetting('photo_sphere_viewer_image_style'),
        '#empty_option' => $this->t('None (original image)'),
        '#description' => $description_link->toRenderable(),
        ];

        $element['show_navbar'] = [
        '#title' => $this->t('Show Navbar'),
        '#type' => 'checkbox',
        '#description' => $this->t('Select the Checkbox to show Navbar.'),
        '#default_value' => $this->getSetting('show_navbar'),
        ];

        $element['enable_mousewheel'] = [
        '#title' => $this->t('Enable MouseWheel navigation option'),
        '#type' => 'checkbox',
        '#description' => $this->t(
            'Select the Checkbox to use MouseWheel
        navigation option.'
        ),
        '#default_value' => $this->getSetting('enable_mousewheel'),
        ];

        $element['use_title_as_caption'] = [
        '#title' => $this->t('Use image title attribute as caption'),
        '#type' => 'checkbox',
        '#description' => $this->t(
            'Select the Checkbox to use image title
         attribute(if available) as caption.'
        ),
        '#default_value' => $this->getSetting('enable_mousewheel'),
        ];
        return $element;

    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary()
    {
        $image_styles = image_style_options(false);
        $summary = [];
        unset($image_styles['']);
    
        $image_style = $this->getSetting('photo_sphere_viewer_image_style');

        if (isset($image_styles[$image_style])) {
            $summary[] = $this->t(
                'Display image style: @style',
                ['@style' => $image_styles[$image_style]]
            );
        } else {
            $summary[] = $this->t('Original image');
        }
        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $image_style = $this->getSetting('photo_sphere_viewer_image_style');
        $show_navbar = $this->getSetting('show_navbar');
        $enable_mousewheel = $this->getSetting('enable_mousewheel');
        $use_title_as_caption = $this->getSetting('use_title_as_caption');

        $element = [];
        foreach ($items as $delta => $item) {

            if ($image_style) {
                $panoramaImageUrl = ImageStyle::load($image_style)
                    ->buildUrl($item->entity->getFileUri());
                $style = ImageStyle::load($image_style);
                $style_configs = $style->getEffects()->getConfiguration();
                foreach ($style_configs as $config) {
                    $height = $config['data']['height'];
                    $width = $config['data']['width'];
                }
            } else {
                $panoramaImageUrl = file_create_url($item->entity->getFileUri());
            }
            // Settings array keep the value of image dimension.
            $settings = [
            'height' => ($height) ?? '400',
            'width' => ($width) ?? '600'
            ];
            $element[$delta] = [
            '#theme' => 'photo_sphere_viewer',
            '#item' => $item,
            '#settings' => $settings,
            '#attached' => [
            'drupalSettings' => [
            'panoramaImageUrl' => $panoramaImageUrl,
            'show_navbar' => $show_navbar,
            'enable_mousewheel' => $enable_mousewheel,
            'use_title_as_caption' => $use_title_as_caption,
            'image_title' => $item->title,
            ],
            ],
            ];
        }
        return $element;
    }

}
